import axios from "axios";
import * as crypto from "crypto";
import { inject, injectable } from "inversify";
import * as _ from "lodash";
import * as moment from "moment-timezone";
import { Transaction } from "sequelize";
import { inspect } from "util";
import ApplicationConstants from "../../local_modules/platform-shared/src/dal/constants/ApplicationConstants";
import { DEVICE_STATUS } from "../../local_modules/platform-shared/src/dal/constants/DeviceStatus";
import { EventId } from "../../local_modules/platform-shared/src/dal/constants/EventId";
import { DeviceDTO } from "../../local_modules/platform-shared/src/dto/device/DeviceDTO";
import DeviceStatusDTO from "../../local_modules/platform-shared/src/dto/device/DeviceStatusDTO";
import { PatientEventPayloadDTO } from "../../local_modules/platform-shared/src/dto/event/PatientEventPayloadDTO";
import { ApplicationEventDispatcher } from "../../local_modules/platform-shared/src/service/event/ApplicationEventDispatcher";
import DeviceUnassignedEvent from "../../local_modules/platform-shared/src/service/event/DeviceUnassignedEvent";
import { EVENT_STATUS_TYPE } from "../constants/EventStatusType";
import TYPES from "../constants/types";
import { DeviceAuthDTO } from "../dto/device/DeviceAuthDTO";
import { IncomingDeviceEventDTO } from "../dto/event/IncomingDeviceEventDTO";
import { IncomingDeviceEventRequestDTO } from "../dto/event/IncomingDeviceEventRequestDTO";
import DeviceLogRequestDTO from "../dto/log/DeviceLogRequestDTO";
import DevicePingDTO from "../dto/status/DevicePingDTO";
import DeviceStatusRequestDTO from "../dto/status/DeviceStatusRequestDTO";
import StatusMeta from "../dto/status/EventMeta";
import { AuthenticationError } from "../errors/AuthenticationError";
import { SERVER_ERRORS as ERROR } from "../errors/SERVER_ERRORS";
import { ServiceError } from "../errors/ServiceError";
import { ValidationError } from "../errors/ValidationError";
import { DeviceLogRepository } from "../repository/DeviceLogRepository";
import { DeviceRepository } from "../repository/DeviceRepository";
import { DeviceStatusRepository } from "../repository/DeviceStatusRepository";
import AxiosHelper from "../util/AxiosHelper";
import AzureFileStorage from "../util/AzureFileStorage";
import AzureServiceBus from "../util/AzureServiceBus";
import Logger from "../util/Logger";
import { DeviceUsageEventService } from "./DeviceUsageEventService";
import { PatientService } from "./PatientService";
import { RegionService } from "./RegionService";

export interface DeviceService {
    findByToken(token: string): Promise<DeviceDTO>;

    /**
     * Checks if the timezone has changed and updates it in the database, if necessary
     *
     * @param device the current device record from the database. Should include the properties Device.deviceTimeZone and Device.timeZoneOffset
     * @param incomingTimeZone the timezone sent by the device
     * @param incomingTimeZoneOffset the timezone offset sent by the device
     * @param transaction the transaction
     */
    checkTimeZone(device: DeviceDTO, incomingTimeZone: string, incomingTimeZoneOffset: number, transaction?: Transaction): Promise<void>;

    updateTimeZone(deviceId: string, timeZone: string, tzOffset: number, transaction?: Transaction): Promise<void>;

    processDeviceLogs(dto: DeviceLogRequestDTO): Promise<void>;
    receiveEvents(request: IncomingDeviceEventRequestDTO): Promise<void>;
    handlePingRequest(requestBody: DevicePingDTO): Promise<void>;
    handleStatusRequest(reqDto: DeviceStatusRequestDTO): Promise<void>;
    unassignDevice(device: DeviceDTO): Promise<void>;

    /**
     * This method is responsible for verifying the device's sim, imei and androidId
     * and to create a token based on those values
     *
     * @param dto DeviceAuthDTO containing sim imei and androidId of a Karie device
     */
    authenticateDevice(dto: DeviceAuthDTO): Promise<string>;

    /**
     * Changes the device region locally and sends over the information to whatever the
     * new region selected is. Returns the url to access the new region.
     *
     * @param region selected new region
     * @param dto device information
     */
    changeRegion(region: string, dto: DeviceDTO): Promise<string>;
    upsertDevice(device: DeviceDTO): Promise<DeviceDTO>;
}

@injectable()
export class DeviceServiceImpl implements DeviceService {
    private applicationEventDispatcher: ApplicationEventDispatcher;
    private deviceEventService: DeviceUsageEventService;
    private deviceRepository: DeviceRepository;
    private deviceLogRepository: DeviceLogRepository;
    private deviceStatusRepository: DeviceStatusRepository;
    private patientService: PatientService;
    private regionService: RegionService;

    @inject(TYPES.DevicePingUpdateInterval)
    private pingUpdateInterval: number;

    constructor(
        @inject(ApplicationConstants.ApplicationEventDispatcher) applicationEventDispatcher: ApplicationEventDispatcher,
        @inject(TYPES.DeviceUsageEventService) deviceEventService: DeviceUsageEventService,
        @inject(TYPES.DeviceRepository) deviceRepository: DeviceRepository,
        @inject(TYPES.DeviceLogRepository) deviceLogRepository: DeviceLogRepository,
        @inject(TYPES.DeviceStatusRepository) deviceStatusRepository: DeviceStatusRepository,
        @inject(TYPES.PatientService) patientService: PatientService,
        @inject(TYPES.RegionService) regionService: RegionService,
    ) {
        this.applicationEventDispatcher = applicationEventDispatcher;
        this.deviceEventService = deviceEventService;
        this.deviceRepository = deviceRepository;
        this.deviceLogRepository = deviceLogRepository;
        this.deviceStatusRepository = deviceStatusRepository;
        this.patientService = patientService;
        this.regionService = regionService;
    }

    public async findByToken(token: string): Promise<DeviceDTO> {
        return await this.deviceRepository.findByToken(token);
    }

    public async checkTimeZone(device: DeviceDTO, incomingTimeZone: string, incomingTimeZoneOffset: number, transaction?: Transaction): Promise<void> {
        // Nothing to do in case the timezone has not changed
        if (device.$deviceTimeZone === incomingTimeZone && device.$timeZoneOffset === incomingTimeZoneOffset) {
            return;
        }
        await this.updateTimeZone(device.$deviceId, incomingTimeZone, incomingTimeZoneOffset);
    }

    public async updateTimeZone(deviceId: string, timeZone: string, tzOffset: number, transaction?: Transaction): Promise<void> {
        await this.deviceRepository.updateTimeZone(deviceId, timeZone, tzOffset, transaction);
    }

    public async processDeviceLogs(dto: DeviceLogRequestDTO): Promise<void> {
        const date = moment().format("YYYY_MM_DD_HH_mm");
        const serial = dto.$device.$deviceSerialNumber;
        if (!dto.$fileName) {
            dto.$fileName = `DEVICE_${serial}_${date}.zip`;
        } else {
            dto.$fileName = dto.$fileName.replace(".zip", `DEVICE_${serial}_${date}.zip`);
        }
        try {
            const logDto = await AzureFileStorage.uploadDeviceLog(dto);
            await this.deviceLogRepository.create(logDto);
            if (dto.$device.$logRequest) {
                await this.deviceLogRepository.removeRequest(dto.$device.$deviceId);
                await AzureServiceBus.cancelMessage(dto.$device.$logRequest.$queueReference);
            }
        } catch (error) {
            Logger.error("Could no process log file received from device: " + dto.$device.$deviceId, error.stack);
            throw new ServiceError("Could no process log file received", ERROR.COULD_NOT_PROCESS_LOG_FILE);
        }
    }

    public async receiveEvents(request: IncomingDeviceEventRequestDTO): Promise<void> {
        const device = request.device;
        // The device is added by a middleware, so if reaches here without a device, it could be a problem on the middleware
        if (!device) {
            throw new ServiceError("Internal server error", ERROR.UNKNOWN_SERVER_ERROR);
        }
        // Just to make sure the events are ordered by dispatched moment
        const incomingEvents = _.sortBy(request.events, "dispatchedAt" as keyof IncomingDeviceEventDTO);

        for (const incomingEvent of incomingEvents) {
            const event = IncomingDeviceEventDTO.toDeviceUsageEventDTO(device, incomingEvent);
            // Store the event into database
            await this.deviceEventService.createFromDTO(event);
            // Dispatch the event to be handled by the listeners
            await this.deviceEventService.dispatch(event);
        }
    }

    public async handlePingRequest(requestBody: DevicePingDTO): Promise<void> {
        const device = requestBody.device;
        if (!device) {
            Logger.info(`DeviceService.ping: Device should be populated on the object: ${inspect(requestBody)}`);
            throw new ServiceError("Internal server error", ERROR.UNKNOWN_SERVER_ERROR);
        }

        const status = new DeviceStatusDTO();
        const now = new Date();
        status.$deviceId = device.$deviceId;
        status.$networkConnected = true;
        status.$lastNetworkRequest = now;
        if (requestBody.wifiConnected) {
            status.$wifiPingUtc = now;
        }

        if (device.$status && device.$status.$deviceId) {
            const currentStatus = device.$status;
            if (currentStatus.$lastNetworkRequest) {
                const diff = (status.$lastNetworkRequest.valueOf() - currentStatus.$lastNetworkRequest.valueOf()) / 1000;
                // We can save a few unnecessary database updates with this logic
                if (diff < this.getPingUpdateInterval()) {
                    return;
                }
            }
        }

        await this.deviceStatusRepository.createOrUpdate(device, status);
    }

    public async handleStatusRequest(reqDto: DeviceStatusRequestDTO): Promise<void> {
        const device = reqDto.$device;
        if (!device) {
            Logger.info(`On statusUpdate. Device should be populated on the object: ${JSON.stringify(reqDto)}`);
            throw new ServiceError("Internal server error", ERROR.UNKNOWN_SERVER_ERROR);
        }
        if (!device.$patient || !device.$patient.$patientId) {
            Logger.info("On statusUpdate. Device not assigned to a patient: " + device.$deviceId);
        }

        const status = new DeviceStatusDTO();
        if (reqDto.$events) {
            status.$device = device;
            status.$deviceId = device.$deviceId;
            const events = reqDto.$events.length > 1 ? this.sortAndRemove(reqDto.$events) : reqDto.$events;

            for (const event of events) {
                const eventStatus = event.$status;
                if (eventStatus.toUpperCase() as keyof typeof DEVICE_STATUS) {
                    if (event.$type === EVENT_STATUS_TYPE.UPDATE) {
                        if (eventStatus === DEVICE_STATUS.DEVICE_ON_WIFI) {
                            status.$wifiPingUtc = new Date();
                            continue;
                        }
                        if (eventStatus.indexOf("BATTERY_") === 0) {
                            status.$batteryStatus = eventStatus;
                            status.$batteryStatusUtc = event.$dateTime;
                            continue;
                        }
                        if (eventStatus.indexOf("CARTRIDGE_") === 0) {
                            status.$cartridgeStatus = eventStatus;
                            status.$cartridgeStatusUtc = event.$dateTime;
                            continue;
                        }
                        if (eventStatus === DEVICE_STATUS.DEVICE_PLUGGED_IN) {
                            status.$pluggedIn = true;
                            status.$pluggedInUtc = event.$dateTime;
                            continue;
                        } else if (eventStatus === DEVICE_STATUS.DEVICE_UNGLUGGED) {
                            status.$pluggedIn = false;
                            status.$pluggedInUtc = event.$dateTime;
                            continue;
                        }
                    } else if (event.$type === EVENT_STATUS_TYPE.ERROR) {
                        // TODO: implement logic for errors
                        Logger.warn("DEVICE ---- " + device.$deviceId + " REPORTED ERROR: " + eventStatus + " ON (device time): "
                            + moment(event.$dateTime).tz(device.$deviceTimeZone).format("YYYY-MM-DD HH:mm:ss:SSS") + " WITH MESSAGE: " + event.$message);
                        continue;
                    }
                } else {
                    Logger.warn("DEVICE ---- " + device.$deviceId + " SENT AN INVALID STATUS EVENT: " + eventStatus + " ON (device time): "
                        + moment(event.$dateTime).tz(device.$deviceTimeZone).format("YYYY-MM-DD HH:mm:ss:SSS") + " WITH MESSAGE: " + event.$message);
                }
            }
        }

        try {
            await this.deviceStatusRepository.createOrUpdate(device, status);
        } catch (error) {
            throw new ServiceError("Could not update device status.", ERROR.COULD_NOT_UPDATE_DEVICE_STATUS);
        }
    }

    public async unassignDevice(device: DeviceDTO): Promise<void> {
        if (!device || !device.$status) {
            device = await this.deviceRepository.findByToken(device.$token);
        }

        if (!device.$status || !device.$status.$unassignReady) {
            Logger.info("DEVICE ---- " + device.$deviceId + " IS NOT READY TO UNASSIGN ");
            throw new ValidationError("Not ready for unassignement.", ERROR.NOT_READY_FOR_UNASSIGNEMENT);
        }

        const patient = device.$patient;
        await this.patientService.unassignDevice(device.$deviceId);
        await this.deviceEventService.create(EventId.DeviceUnassigned, device.$deviceId, null);
        await this.applicationEventDispatcher.dispatch(new DeviceUnassignedEvent(this, new PatientEventPayloadDTO(device, patient)));
    }

    public async authenticateDevice(dto: DeviceAuthDTO): Promise<string> {
        const device = await this.deviceRepository.findBySimImei(dto.$imei, dto.$simNumber).catch(() => {
            throw new AuthenticationError(ERROR.INVALID_CREDENTIALS);
        });

        if (device.$androidId && device.$androidId !== null && device.$androidId !== dto.$androidId) {
            throw new AuthenticationError(ERROR.INVALID_CREDENTIALS);
        }

        if (device.$token) {
            Logger.warn(`On authenticateDevice. Device with id: ${device.$deviceId} already has a token:
            ${device.$token} already has an androidId: ${device.$androidId}`);
        } else {
            device.$androidId = dto.$androidId;
            device.$token = this.createDeviceAuthToken(device);

            const token = await this.deviceRepository.updateWithTokenAndAndroidId(device);

            if (token !== device.$token) {
                throw new ServiceError("Process could not be completed", ERROR.UNKNOWN_SERVER_ERROR);
            }
        }

        return device.$token;
    }

    public async changeRegion(region: string, dto: DeviceDTO): Promise<string> {
        // Verify if the region exists and get url for the karie api on that region
        const reg = await this.regionService.findByName(region, true);
        let previousRegionUrl;
        const currentRegion = dto.$region;

        if (reg && reg.$applications && reg.$applications.length > 0) {
            const newRegionUrl = reg.$applications[0].$baseUrl;

            if (dto.$regionId !== reg.$id) {
                let dtoRegion = dto.$region;
                // If the device is not supposed to be connecting to this REGION but wants to connect to it
                // we should update the previous REGION it was supposed to connect to. This piece of code
                // is to get the url of the previous region.
                const allowedRegions = process.env.API_REGION && process.env.API_REGION !== "" ? process.env.API_REGION.split(",") : [];
                if (dtoRegion && allowedRegions && allowedRegions.length > 0 && allowedRegions.indexOf(dtoRegion.$name) < 0) {
                    if (!dtoRegion.$applications || dtoRegion.$applications.length === 0) {
                        const previousRegion = await this.regionService.findByName(allowedRegions[0]!, true);
                        if (previousRegion) {
                            dtoRegion = previousRegion;
                        } else {
                            Logger.error(`On changeRegion. We were not able to find the
                            previous region url for device id: ${dto.$deviceId} s/n: ${dto.$deviceSerialNumber}`);
                            throw new ServiceError("It was not possible to complete the region update", ERROR.UNKNOWN_SERVER_ERROR);
                        }
                    }
                    if (dtoRegion.$applications && dtoRegion.$applications.length > 0
                        && dtoRegion.$applications[0].$baseUrl.indexOf("TEST_ONLY_URL") < 0) {
                        previousRegionUrl = dtoRegion.$applications[0].$baseUrl;
                    }
                }
                dto.$region = reg;

                // send device information over to that region
                await this.sendDeviceInfoToRegion(this.normalizeUrl(previousRegionUrl || newRegionUrl), dto);

                // update device's current region
                const dev = await this.deviceRepository.updateRegion(dto.$deviceId, reg.$id);

                if (!dev || dev.$regionId !== reg.$id) {
                    Logger.error(`On changeRegion. It was not possible to complete the
                    region update for device id: ${dto.$deviceId} s/n: ${dto.$deviceSerialNumber}`);

                    dto.$region = currentRegion;
                    await this.sendDeviceInfoToRegion(this.normalizeUrl(previousRegionUrl || newRegionUrl), dto);

                    throw new ServiceError("It was not possible to complete the region update", ERROR.COULD_NOT_UPDATE_DEVICE_REGION);
                }
            }

            return newRegionUrl;
        }

        throw new ServiceError("It was not possible to complete the request to change the region.", ERROR.UNKNOWN_SERVER_ERROR);
    }

    public async upsertDevice(device: DeviceDTO): Promise<DeviceDTO> {
        if (device.$region) {
            const region = await this.regionService.findByName(device.$region.$name);
            if (region) {
                device.$regionId = region.$id;
            } else {
                throw new ValidationError(`Region ${device.$region.$name} does not exist in this enviroment.`, ERROR.COULD_NOT_FIND_REGION);
            }
        }
        const dev = await this.deviceRepository.upsertDevice(device);
        if (!dev || !dev.$deviceId) {
            throw new ServiceError("It was not possible to upsert the device in this region.", ERROR.UNKNOWN_SERVER_ERROR);
        }
        return dev;
    }

    private async sendDeviceInfoToRegion(regionUrl: string, device: DeviceDTO) {
        try {
            const resp = await axios.put(`${regionUrl}api_auth/device/`, device, {
                headers: {
                    Authorization: `Bearer ${process.env.API_TOKEN}`,
                },
                timeout: 30000,
            });
            if (resp && resp.status === 200) {
                Logger.info(`Succesfully sent device id: ${device.$deviceId} s/n: ${device.$deviceSerialNumber} to ${regionUrl}`);
            } else {
                throw new Error("Unexpected response from the api or axios issue.");
            }
        } catch (err) {
            const errMsg = `It was not possible to send over the device id: ${device.$deviceId} s/n: ${device.$deviceSerialNumber} to ${regionUrl}`;
            AxiosHelper.handleError(err, errMsg);
            throw new ServiceError("It was not possible to complete the request to change the region.", ERROR.UNKNOWN_SERVER_ERROR);
        }
    }

    private normalizeUrl(regionUrl: string) {
        regionUrl = regionUrl.replace("TEST_ENV_REMOVE", "");
        let url = regionUrl.endsWith("/") ? regionUrl : `${regionUrl}/`;
        if (url.indexOf("v1/") < 0) {
            url += "v1/";
        }
        return url;
    }

    /**
     * This method is responsible for forging a token for a device
     *
     * @param device DeviceDTO containing necessary data for forging a token
     */
    private createDeviceAuthToken(device: DeviceDTO): string {
        return crypto.createHmac("sha256", device.$salt)
            .update(device.$deviceId + device.$androidId + process.env.KARIE_SECRET_KEY + Date.now()).digest("base64");
    }

    /**
     * This method is to guaranteee that we ony process one status update of each type in case the device
     * sends multiple we will give preference to the latest (based on its dateTime) of that same type.
     * @param meta Meta data sent by the device contaning the status and the event time.
     */
    private sortAndRemove(meta: StatusMeta[]) {
        const groupedEvents = _.groupBy(meta, (m) => m.$status);
        let newMeta: StatusMeta[] = [];

        for (const group in groupedEvents) {
            if (groupedEvents.hasOwnProperty(group)) {
                let values = groupedEvents[group];
                if (values.length > 1 && values[0].$type === EVENT_STATUS_TYPE.UPDATE) {
                    values = _.sortBy(values, "dateTime");
                    newMeta.push(values[0]);
                    continue;
                }
                newMeta = newMeta.concat(values);
            }
        }
        return newMeta;
    }

    /**
     * Returns the interval, in seconds, the device should be updated when receiving a ping request
     */
    private getPingUpdateInterval(): number {
        return this.pingUpdateInterval;
    }

}
